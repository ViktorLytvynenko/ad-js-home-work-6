///Theory
//Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
//Это значит, что в задании стоит одна функция и другие задачи не будут выполнены до завершения текущей функции.

///Practice

const ipLinkGet = "https://api.ipify.org/?format=json";
const ipLinkLocation = "http://ip-api.com/json/";
const params = "?fields=status,message,continent,country,region,city,district,query"
const buttonElement = document.querySelector("#button");
const list = document.querySelector("#list");

const generateInfo = (field) => {
    list.innerHTML = `<li>Continent: ${field.continent ? field.continent : "Not found!"}</li>`;
    list.innerHTML += `<li>Country: ${field.country ? field.country : "Not found!"}</li>`;
    list.innerHTML += `<li>Region: ${field.region ? field.region : "Not found!"}</li>`;
    list.innerHTML += `<li>City: ${field.city ? field.city : "Not found!"}</li>`;
    list.innerHTML += `<li>District: ${field.district ? field.district : "Not found!"}</li>`;
}


const getIP = async () => {
    const res = await axios.get(ipLinkGet);
    const ip = res.data.ip;
    getLocation(ip);
}

const getLocation = async (ipUser) => {
    const res = await axios.get(`${ipLinkLocation}${ipUser}${params}`);
    generateInfo(res.data);
}

buttonElement.addEventListener("click", () => {
    getIP()
})